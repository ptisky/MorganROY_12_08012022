# Sportsee - P12 Openclassrooms Project

This project use an API to get informations from stored data in JSON to fill charts

## Dependencies

- [React] v17.0.2
- [recharts] v2.0.10
- [axios] v0.21.1
- [prop-types] v17.7.2

### Installation

- You need Git to clone the repository
- You need Node to run the npm commands

## Install and run the project

1. Clone the repository of SportSee back-end: - git clone https://gitlab.com/ptisky/MorganROY_12_08012022.git
2. Enter in the back : - cd API
3. Inside API repository, install dependencies: - npm install
4. Launch API on port 3000 (default port): - npm run start
5. go back to the project : - cd ..
6. install dependencies : - npm install
7. run the project : - npm run start (ask to open in 3001 port)

