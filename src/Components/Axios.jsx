import { useState, useEffect } from "react";
import axios from "axios";
import PropTypes from "prop-types";

/**
 * Use to get information from API
 * @param url
 * @returns {unknown}
 */
const useAxios = (url) => {
    const [data, setData] = useState(undefined);
    useEffect(() => {
        (async () => {
            const response = await axios.get(
                `http://${window.location.hostname}:3000/${url}`
            );

            if (response.status === 200) setData(response.data.data);
        })();
    }, [url]);
    return data;
};

useAxios.propTypes = {
    url: PropTypes.string.isRequired,
};

export default useAxios;
