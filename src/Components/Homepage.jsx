import { useContext } from "react";
import { IdContext } from "./Context.jsx";
import useAxios from "./Axios.jsx";
import UserActivity from "./Activity.jsx";
import UserSession from "./SessionTime.jsx";
import UserPerform from "./Performance.jsx";
import UserScore from "./Objectif.jsx";
import UserCard from "./Cards.jsx";
import iconError from "../assets/iconError.png";
import "../css/theme.css";

/**
 * Display homepage modules
 * @returns {JSX.Element}
 * @constructor
 */
const Dashboard = () => {
    const userId = useContext(IdContext);
    const userData = useAxios(`user/${userId}`);
    return (
        <div className="main_body">
            {userData ? (
                <section className="homepage">
                    <div className="intro_infos">
                        <h2>
                            Bonjour<span>{userData.userInfos.firstName}</span>
                        </h2>
                        <p>
                            Félicitation ! Vous avez explosé vos objectifs hier !
                        </p>
                    </div>

                    <UserActivity />

                    {[0, 1, 2, 3].map((i) => (
                        <UserCard key={i} category={i} />
                    ))}

                    <UserSession />
                    <UserPerform />
                    <UserScore />
                </section>
            ) : (
                <div className="DashboardError">
                    <img src={iconError} alt="Erreur" />
                    <p>Server disconnected</p>
                </div>
            )}
        </div>
    );
};

export default Dashboard;
