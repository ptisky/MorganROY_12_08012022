import { useContext } from "react";
import { IdContext } from "./Context";
import useAxios from "./Axios";
import {
    ResponsiveContainer,
    Radar,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
} from "recharts";
import "../css/theme.css";

/**
 * Run performance module
 * @returns {JSX.Element}
 * @constructor
 */
const UserPerform = () => {
    const userId = useContext(IdContext);
    let userPerform = useAxios(`user/${userId}/performance`);

    //userPerform = null;

    if (userPerform) {
        const arrayKind = [
            "Cardio",
            "Energie",
            "Endurance",
            "Force",
            "Vitesse",
            "Intensité",
        ];

        for (let i = 0; i < userPerform.data.length; i++) {
            const intKind = userPerform.data[i].kind;
            if (Number.isInteger(intKind))
                userPerform.data[i].kind = arrayKind[intKind - 1];
        }
    }

    return (
        <article className="UserPerform">
            {userPerform ? (
                <ResponsiveContainer width="100%" height="100%">
                    <RadarChart
                        innerRadius={12.5}
                        outerRadius="69%"
                        data={userPerform.data}
                    >
                        <PolarGrid />
                        <PolarAngleAxis dataKey="kind" />
                        <Radar
                            dataKey="value"
                            stroke="#FF0101"
                            fill="#FF0101"
                            fillOpacity={0.64}
                            animationBegin={3100}
                            animationDuration={1900}
                        />
                    </RadarChart>
                </ResponsiveContainer>
            ) : (
                <div className="loadingState">
                    <p>Chargement...</p>
                </div>
            )}
        </article>
    );
};

export default UserPerform;
