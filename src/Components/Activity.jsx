import { useContext } from "react";
import { IdContext } from "./Context";
import useAxios from "./Axios";

import {
    ResponsiveContainer,
    BarChart,
    Bar,
    XAxis,
    YAxis,
    Tooltip,
    CartesianGrid,
} from "recharts";
import "../css/theme.css";


const UserActivity = () => {

    const userId = useContext(IdContext);
    const userActivity = useAxios(`user/${userId}/activity`);

    if (userActivity) {

        for (let i = 0; i < userActivity.sessions.length; i++) {
            const day = userActivity.sessions[i].day.toString();
            const words = day.split('-');
            if(words[1]){
                userActivity.sessions[i].day = words[2] + "/" + words[1];
            }
        }

    }

    const CustomTooltip = ({ active, payload }) => {
        if (active) {
            return (
                <div className="custom-tooltipActivity">
                    <p>{`${payload[0].value}kg`}</p>
                    <p>{`${payload[1].value}Kcal`}</p>
                </div>
            );
        }

        return null;
    };

    return (
        <article className="UserActivity">
            <div className="legendActivity">
                <h4>Activité quotidienne</h4>
                <ul>
                    <li>Poids (kg)</li>
                    <li>Calories brûlées (kCal)</li>
                </ul>
            </div>
            {userActivity ? (
                <ResponsiveContainer>
                    <BarChart data={userActivity.sessions} barGap={9}>
                        <XAxis dataKey="day" dy={12.5} stroke={"#9B9EAC"} />
                        <YAxis
                            dataKey="calories"
                            tickCount={3}
                            orientation="right"
                            dx={17}
                            stroke={"#9B9EAC"}
                        />
                        <Tooltip content={<CustomTooltip />} />
                        <CartesianGrid stroke="#DEDEDE" strokeDasharray="4 4" />
                        <Bar
                            dataKey="kilogram"
                            id="barkg"
                            fill="#000"
                            barSize={7}
                            radius={[3.5, 3.5, 0, 0]}
                            animationBegin={2800}
                            animationDuration={1600}
                        />
                        <Bar
                            dataKey="calories"
                            id="barkCal"
                            fill="#E60000"
                            barSize={7}
                            radius={[3.5, 3.5, 0, 0]}
                            animationBegin={3000}
                            animationDuration={1900}
                        />
                    </BarChart>
                </ResponsiveContainer>
            ) : (
                <div className="loadingState">
                    <p>Chargement...</p>
                </div>
            )}
        </article>
    );
};

export default UserActivity;
