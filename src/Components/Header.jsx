import logo from "../assets/logo2.png";
import "../css/theme.css";

const Header = () => {
    return (
        <header>
            <h1 tabIndex="0">
                <img src={logo} alt="Logo SportSee" />
            </h1>

            <nav>
                <ul>
                    <li tabIndex="0">
                        <a href="/" title="Accueil">
                            Accueil
                        </a>
                    </li>
                    <li tabIndex="1">
                        <a href="/" title="Profil">
                            Profil
                        </a>
                    </li>
                    <li tabIndex="2">
                        <a href="/" title="Réglage">
                            Réglage
                        </a>
                    </li>
                    <li tabIndex="3">
                        <a href="/" title="Communauté">
                            Communauté
                        </a>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Header;
