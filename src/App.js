import { IdContext } from "./Components/Context.jsx";
import Header from "./Components/Header.jsx";
import Aside from "./Components/Aside.jsx";
import Homepage from "./Components/Homepage.jsx";
import "./css/theme.css";

const App = () => {
    return (
        <main className="App">
            <Header />
            <Aside />
            <IdContext.Provider value={"18"}>
                <Homepage />
            </IdContext.Provider>
        </main>
    );
};

export default App;
